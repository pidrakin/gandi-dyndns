package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/NYTimes/gziphandler"
	"io/ioutil"
	logger "log"
	"net/http"
	"net/url"
	"os"
	"strconv"
	"time"
)

var version string
var buildTime string

type LogLevel string

const (
	DEBU LogLevel = "DEBUG "
	INFO          = "INFO  "
	WARN          = "WARN  "
	ERRO          = "ERROR "
	FATA          = "FATAL "
)

func log(level LogLevel, format string, v ...interface{}) {
	l := logger.New(os.Stdout, "", 0)
	l.SetPrefix(time.Now().Format("2006-01-02 15:04:05") + " [Gandi DynDNS] ")
	if level == FATA {
		l.Fatalf(string(level)+format, v...)
	} else {
		l.Printf(string(level)+format, v...)
	}
}

func debug(format string, v ...interface{}) {
	log(DEBU, format, v...)
}

func info(format string, v ...interface{}) {
	log(INFO, format, v...)
}

func warn(format string, v ...interface{}) {
	log(WARN, format, v...)
}

func error(format string, v ...interface{}) {
	log(ERRO, format, v...)
}

func fatal(format string, v ...interface{}) {
	log(FATA, format, v...)
}

type StatusType string

const (
	OK    StatusType = "OK"
	ERROR            = "ERROR"
)

type Response struct {
	Status StatusType `json:"status"`
	Result string     `json:"result"`
}

func enableCors(w *http.ResponseWriter) {
	(*w).Header().Set("Access-Control-Allow-Headers", "*")
	(*w).Header().Set("Access-Control-Allow-Origin", "*")
}

func response(w http.ResponseWriter, code int, format string, v ...interface{}) {
	force200Env := os.Getenv("FORCE_200")
	force200, _ := strconv.ParseBool(force200Env)
	if force200 == true && code < 300 {
		code = 200
	}

	var status StatusType
	if code < 400 {
		status = OK
	} else {
		status = ERROR
		error(format, v...)
	}
	res := Response{
		Status: status,
		Result: fmt.Sprintf(format, v...),
	}

	w.WriteHeader(code)
	js, err := json.Marshal(res)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		fatal(err.Error())
		return
	}

	if _, err = w.Write(js); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		fatal(err.Error())
		return
	}
}

func handler(w http.ResponseWriter, r *http.Request) {
	enableCors(&w)
	w.Header().Set("Content-Type", "application/json")

	decodedValue, err := url.QueryUnescape(r.RequestURI)
	if err != nil {
		response(w, http.StatusInternalServerError, "cannot decode uri: %s", err.Error())
		return
	}
	debug("received: %s [%s] %s\n", r.Proto, r.Method, decodedValue)
	query := r.URL.Query()
	apiKey := query.Get("apiKey")
	fqdn := query.Get("fqdn")
	name := query.Get("name")
	ipaddr := query.Get("ipaddr")
	recordType := query.Get("type")
	ttl := query.Get("ttl")

	if apiKey == "" {
		response(w, http.StatusBadRequest, "bad request: query parameter <apiKey> is mandatory")
		return
	}
	if fqdn == "" {
		response(w, http.StatusBadRequest, "bad request: query parameter <fqdn> is mandatory")
		return
	}
	if name == "" {
		response(w, http.StatusBadRequest, "bad request: query parameter <name> is mandatory")
		return
	}
	if ipaddr == "" {
		response(w, http.StatusBadRequest, "bad request: query parameter <ipaddr> is mandatory")
		return
	}
	if recordType == "" {
		recordType = "A"
	}
	if ttl == "" {
		ttl = "3600"
	}

	postBody := []byte(fmt.Sprintf(`{
  "rrset_values": ["%s"],
  "rrset_ttl": %s
}`, ipaddr, ttl))

	gandiUrl := fmt.Sprintf("https://api.gandi.net/v5/livedns/domains/%s/records/%s/%s", fqdn, name, recordType)

	req, err := http.NewRequest(http.MethodPut, gandiUrl, bytes.NewBuffer(postBody))
	if err != nil {
		response(w, http.StatusInternalServerError, "could not create HTTP request: %s", err.Error())
		return
	}
	req.Header.Add("Authorization", "Apikey "+apiKey)
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Accept", `application/json`)

	client := &http.Client{}
	res, err := client.Do(req)
	//Handle Error
	if err != nil {
		response(w, http.StatusInternalServerError, "could not send HTTP request to gandi: %s", err.Error())
		return
	}
	defer res.Body.Close()
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		response(w, http.StatusInternalServerError, "could not retrieve body from gandi HTTP request: %s", err.Error())
		return
	}
	response(w, res.StatusCode, "[Gandi Response] %s", string(body))
}

func getPort() string {
	port := os.Getenv("PORT")
	if port != "" {
		_, err := strconv.Atoi(port)
		if err != nil {
			port = "8080"
		}
	} else {
		port = "8080"
	}
	return port
}

func main() {
	port := getPort()
	info("version: %s", version)
	info("build time: %s", buildTime)
	info("started on port %s\n", port)
	withoutGz := http.HandlerFunc(handler)
	withGz := gziphandler.GzipHandler(withoutGz)
	http.Handle("/", withGz)
	fatal(http.ListenAndServe(fmt.Sprintf(":%s", port), nil).Error())
}
